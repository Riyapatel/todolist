package com.example.todolist.Activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.example.todolist.R;
import com.example.todolist.SqliteDatabase.Database_Helper;
import com.example.todolist.SqliteDatabase.Model.Task_Model;
import com.example.todolist.Utils.allcontrols;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Add_New_task extends AppCompatActivity {

    @BindView(R.id.edttask)
    TextInputEditText edttask;
    @BindView(R.id.edtdate)
    TextInputEditText edtdate;
    @BindView(R.id.btnadd)
    Button btnadd;
    @BindView(R.id.imgdelete)
    ImageView imgdelete;

    Database_Helper database;
    List<Task_Model> gettasklist = new ArrayList<Task_Model>();

    String type = "";
    Task_Model task_model;
    Dialog deletedialog;
    Button btncancel, btndelete;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newtask);

        ButterKnife.bind(this);

        allcontrols.setdatepicker_validation1(edtdate, Add_New_task.this);

        database = new Database_Helper(Add_New_task.this);

        type = getIntent().getExtras().getString("type");

        if (type.equalsIgnoreCase("add")) {
            btnadd.setText("ADD");
            imgdelete.setVisibility(View.GONE);
        } else {
            btnadd.setText("UPDATE");
            task_model = (Task_Model) getIntent().getSerializableExtra("model");
            edtdate.setText(task_model.getTaskdate().toString() + "");
            edttask.setText(task_model.getTaskname().toString() + "");
            imgdelete.setVisibility(View.VISIBLE);
        }

        imgdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteDialog();
            }
        });


        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edttask.getText().toString().equalsIgnoreCase("")) {
                    edttask.setError("Required");
                    edttask.requestFocus();
                } else if (edtdate.getText().toString().equalsIgnoreCase("")) {
                    edtdate.setError("Required");
                    edttask.requestFocus();
                } else {

                    if (type.equalsIgnoreCase("add")) {
                        database.insertData(new Task_Model(edttask.getText().toString(), edtdate.getText().toString(), "0"));
                        Toast.makeText(Add_New_task.this, "Task Added Sucessfully", Toast.LENGTH_LONG).show();
                        finish();
                        overridePendingTransition(R.anim.slide_enter, R.anim.slide_out);
                    } else {
                        Task_Model model = new Task_Model(edttask.getText().toString().trim(), edtdate.getText().toString().trim(), task_model.getIsfinished());
                        model.setId(task_model.getId());
                        database.updateContact(model);
                        Toast.makeText(Add_New_task.this, "Task Updated Sucessfully", Toast.LENGTH_LONG).show();

                    }

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_enter, R.anim.slide_out);
    }


    public void DeleteDialog() {

        deletedialog = new Dialog(Add_New_task.this);
        deletedialog.setContentView(R.layout.dialog_delete);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(deletedialog.getWindow().getAttributes());
        int dialogWindowWidth = (int) (displayWidth * 0.8f);
        int dialogWindowHeight = (int) (displayHeight * 0.75f);
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.CENTER;
        deletedialog.getWindow().setAttributes(layoutParams);

        deletedialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        btndelete = (Button) deletedialog.findViewById(R.id.btndelete);
        btncancel = (Button) deletedialog.findViewById(R.id.btncancel);

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (deletedialog.isShowing()) {
                    deletedialog.dismiss();
                }

            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (deletedialog.isShowing()) {
                    deletedialog.dismiss();
                }

                database.deleteContact(task_model.getId());
                finish();
                overridePendingTransition(R.anim.slide_enter, R.anim.slide_out);

            }
        });

        if (!deletedialog.isShowing()) {
            deletedialog.show();
        }

    }
}
