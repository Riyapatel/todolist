package com.example.todolist.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.todolist.Adapter.TaskList_Adapter;
import com.example.todolist.R;
import com.example.todolist.SqliteDatabase.Database_Helper;
import com.example.todolist.SqliteDatabase.Model.Task_Model;
import com.example.todolist.Utils.ListViewUtil;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.fabadd)
    FloatingActionButton fabadd;
    @BindView(R.id.txtempty)
    TextView txtempty;
    @BindView(R.id.app_bar_layout)
    AppBarLayout app_bar_layout;
    @BindView(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.nestedscroll)
    NestedScrollView nestedscroll;
    @BindView(R.id.collapsible_toolbar)
    Toolbar collapsible_toolbar;
    @BindView(R.id.lsttask)
    ListViewUtil lsttask;
    @BindView(R.id.txttoolbar)
    TextView txttoolbar;

    Database_Helper database;
    List<Task_Model> gettasklist = new ArrayList<Task_Model>();
    TaskList_Adapter taskList_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(collapsible_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        collapsingToolbarLayout.setTitle(" ");
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.MyTextViewStyle1);

        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(" ");
                    txttoolbar.setVisibility(View.VISIBLE);
                    nestedscroll.setBackground(getResources().getDrawable(R.drawable.left_cornerstraight));
                    isShow = true;
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 10, 0, 0);
                    lsttask.setLayoutParams(params);

                } else if (isShow) {
                    txttoolbar.setVisibility(View.GONE);
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    nestedscroll.setBackground(getResources().getDrawable(R.drawable.left_corner));
                    isShow = false;
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, 100, 0, 0);
                    lsttask.setLayoutParams(params);
                    //lnpadding.setPadding(10, 60, 10, 10);

                }
            }
        });


        fabadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, Add_New_task.class);
                intent.putExtra("type", "add");
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        gettasklist.clear();
        database = new Database_Helper(MainActivity.this);
        gettasklist = database.getAllData();
        lsttask.setEmptyView(txtempty);

        taskList_adapter = new TaskList_Adapter(MainActivity.this, gettasklist);
        lsttask.setAdapter(taskList_adapter);

    }
}
