package com.example.todolist.Utils;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class allcontrols extends Fragment {

    public static void setdatepicker_validation1(final EditText e1, final Context context) {

        e1.setFocusable(false);
        e1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                            @Override
                            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                int num = monthOfYear + 1;
                                String text = (num < 10 ? "0" : "") + num;
                                int num1 = dayOfMonth;
                                String text1 = (num1 < 10 ? "0" : "") + num1;

                                e1.setText(getdate(text1 + "-" + (text) + "-" + year));

                            }
                        })
                        .setFirstDayOfWeek(Calendar.SUNDAY)
                        .setDateRange(null, null)
                        .setDoneText("Yes")
                        .setCancelText("No");

                if (e1.getText().toString().equals("")) {
                    cdp.setPreselectedDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                } else {
                    String datestring = e1.getText().toString();
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        Date date = format.parse(datestring);

                        Calendar c1 = Calendar.getInstance();
                        c1.setTime(date);
                        cdp.setPreselectedDate(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DAY_OF_MONTH));
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

                FragmentActivity activity = (FragmentActivity) context;
                cdp.show(activity.getSupportFragmentManager(), "date");

            }
        });
    }

    public static String getdate(String date) {
        String dt = date;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
            Date dateObj = sdf.parse(date);

            dt = new SimpleDateFormat("dd/MM/yyyy").format(dateObj);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dt;
    }


    public static String comparedate(String s) {

        String date = "";

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);

        if (s.equalsIgnoreCase(formattedDate)) {
            date = "Today";
        } else {
            date = s;
        }

        return date;

    }


}
