package com.example.todolist.SqliteDatabase.Model;

import java.io.Serializable;

public class Task_Model implements Serializable {

    String taskname, taskdate;
    String isfinished;
    int id;

    public Task_Model(String taskname, String taskdat, String isfinished) {
        this.taskname = taskname;
        this.taskdate = taskdat;
        this.isfinished = isfinished;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public String getTaskdate() {
        return taskdate;
    }

    public void setTaskdate(String taskdat) {
        this.taskdate = taskdat;
    }

    public String getIsfinished() {
        return isfinished;
    }

    public void setIsfinished(String isfinished) {
        this.isfinished = isfinished;
    }

    @Override
    public String toString() {
        return "Task_Model{" +
                "taskname='" + taskname + '\'' +
                ", taskdate='" + taskdate + '\'' +
                ", isfinished='" + isfinished + '\'' +
                ", id=" + id +
                '}';
    }
}
