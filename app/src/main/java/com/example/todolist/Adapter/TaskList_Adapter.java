package com.example.todolist.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.example.todolist.Activity.Add_New_task;
import com.example.todolist.Activity.MainActivity;
import com.example.todolist.R;
import com.example.todolist.SqliteDatabase.Database_Helper;
import com.example.todolist.SqliteDatabase.Model.Task_Model;
import com.example.todolist.Utils.allcontrols;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskList_Adapter extends BaseAdapter {


    Context context;
    LayoutInflater inflater;
    List<Task_Model> item = new ArrayList<>();
    Database_Helper database;


    public TaskList_Adapter(Context context, List<Task_Model> item) {
        this.item = item;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        database = new Database_Helper(context);
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_task, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtdate.setText(allcontrols.comparedate(item.get(position).getTaskdate()));
        holder.taskname.setText(item.get(position).getTaskname());

        holder.cardclick.setTag(position);
        holder.cardclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos= (int) view.getTag();

                Intent intent = new Intent(context, Add_New_task.class);
                intent.putExtra("type", "edit");
                intent.putExtra("model", item.get(pos));
                context.startActivity(intent);
                ((Activity) context).overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
            }
        });

        holder.checkfinished.setTag(position);

        if (item.get(position).getIsfinished().equalsIgnoreCase("0")) {
            holder.checkfinished.setChecked(false);
        } else {
            holder.checkfinished.setChecked(true);
        }

        holder.checkfinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int pos = (int) view.getTag();

                if (item.get(pos).getIsfinished().equalsIgnoreCase("0")) {
                    item.get(pos).setIsfinished("1");
                } else {
                    item.get(pos).setIsfinished("0");
                }


                database.updateContact(item.get(pos));
                notifyDataSetChanged();


            }
        });

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.taskname)
        TextView taskname;
        @BindView(R.id.txtdate)
        TextView txtdate;
        @BindView(R.id.checkfinished)
        CheckBox checkfinished;
        @BindView(R.id.cardclick)
        CardView cardclick;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}